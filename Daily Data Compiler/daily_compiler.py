# A script that takes a csv that has been consolidated by the CSV consolidator
# and determines the daily consumption of each meter and the total daily
# consumption across all meters

import pandas as pd

def main():

    dataframe = load_meter_data()
    print dataframe

    dates = get_dates(dataframe)
    meters = get_meters(dataframe)
    columns = get_columns(meters)

    daily_dataframe = pd.DataFrame(columns=columns)

    for date in dates:
        date_data = {}
        for index, row in dataframe.iterrows():
            day = row['datetime'].split('T')[0]
            if day = date:
                

def get_columns(meters):
    columns = list(meters)
    columns.append('date')
    columns.append('total')
    return columns

def get_dates(database):
    datetimes = database['datetime'].tolist()
    dates = []
    for datetime in datetimes:
        dates.append(datetime.split('T')[0])
    dates = list(set(dates))
    return dates

def get_meters(database):
    meters = list(database.columns.values)
    meters.remove('datetime')
    return meters

def load_meter_data():
    # Loads a CSV and converts it to a pandas DataFrame, before returning it
    meter_data = pd.read_csv('data.csv')
    return meter_data

main()
