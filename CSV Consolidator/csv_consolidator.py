# A script that takes a bullshit CSV downloaded from Elink and consolidates
# it into one CSV with the meters as columns, the datetimes as index

import numpy as np
import pandas as pd
from datetime import datetime

def main():
    meter_data = load_meter_data()

    meters = []
    datetimes = []

    # gets a list of all datetimes and a list of meters
    for index, row in meter_data.iterrows():
        datetimes.append(row['DateTime'])
        if row['Identifier'] not in meters:
            meters.append(row['Identifier'])

    datetimes = list(set(datetimes))
    print len(datetimes)
    # finds columns for new dataframe
    columns = meters + ['datetime']

    # generate dataframe with columns
    price_df = pd.DataFrame(columns=columns)

    # put datetimes into empty pandas DF and set as index then sort
    datetimes = pd.Series(datetimes)
    price_df['datetime'] = datetimes.values
    price_df.set_index('datetime', inplace=True)
    price_df.sort_index(inplace=True)


    # populates a list of dictionaries where each dictionary holds the DateTime
    #
    for index, row in meter_data.iterrows():
        if row['Identifier'] in meters:
            try:
                price_df.at[row['DateTime'], row['Identifier']] = row['Value']
            except:
                print("There was an issue with entry ", index, "in the CSV")

    # for item in meter_lists:
    #     price_df.at[item['datetime'], item['meter']] = item['value']

    # price_df = price_df[~price_df.index.duplicated(keep='first')]
    print price_df

    save_database(price_df)

def save_database(database):
    # saves the DataFrame
    file_location = 'final_data.csv'
    database.to_csv(file_location, encoding='utf-8')

def load_meter_data():
    # Loads a CSV and converts it to a pandas DataFrame, before returning it
    meter_data = pd.read_csv('test.csv')
    return meter_data

main()
