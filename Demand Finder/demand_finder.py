import csv
import pandas as pd
import datetime
from dateutil import parser

def get_data():
    data = pd.read_csv('data.csv')
    return data

def find_tariff():
    print('What Tariff are you on?')
    tariff = str(input())
    return tariff

def find_max(data):
    return data.loc[data['Value'].idxmax()]

def get_date(max_val):
    when = max_val['DateTime']
    return when

def find_day(when):
    when = parser.parse(when)
    return when.isoweekday() in range(1,6)

def find_correct_time(when, start, finish):
    when = parser.parse(when)
    if start <= when.hour <= finish:
        return True
    else:
        return False

def delete_line(data, max_val):
    del_line = max_val['DateTime']
    data = data[data.DateTime != del_line]
    return data

def get_tariffs():
    tariff_list = pd.read_csv('tariffs.csv')
    return tariff_list

def weekday_or_allday():
    correct_input = False

    while correct_input != True:
        print('Is this a WEEKDAY or ALLDAY Tariff?')
        day_type = str(raw_input())
        if day_type == 'WEEKDAY' or day_type == 'ALLDAY':
            correct_input = True
        else:
            print("Please type ALLDAY or WEEKDAY exactly")

    return day_type

def weekday_while(data, start_time, end_time):
    isweekday = False
    iscorrecttime = False
    while (isweekday is False and iscorrecttime is False):
        max_val = find_max(data)
        when = get_date(max_val)
        isweekday = find_day(when)
        iscorrecttime = find_correct_time(when, start_time, end_time)
        if (iscorrecttime is False or isweekday is False):
            data = delete_line(data, max_val)
            iscorrecttime = False
            isweekday = False

    return max_val

def allday_while(data, start_time, end_time):
    iscorrecttime = False
    while (iscorrecttime is False):
        max_val = find_max(data)
        when = get_date(max_val)
        iscorrecttime = find_correct_time(when, start_time, end_time)
        if (iscorrecttime is False):
            data = delete_line(data, max_val)
            iscorrecttime = False

    return max_val

def get_start_time():
    print('When is the start of the peak time? (24 Hour Clock)')
    start_time = int(input())
    return start_time

def get_end_time():
    print('When is the end of the peak time? (24 Hour Clock)')
    end_time = int(input())
    return end_time

def main():
    data = get_data()
    day_type = weekday_or_allday()
    start_time = get_start_time()
    end_time = get_end_time()

    if day_type == 'WEEKDAY':
        max_val = weekday_while(data, start_time, end_time)
    else:
        max_val = allday_while(data, start_time, end_time)

    print('THIE MAX VALUE AND TIME IS:')
    print max_val

main()
