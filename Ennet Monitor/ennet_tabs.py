### A script that will open the Ennet sites on the correct page which
### contains all of the charts and information

import webbrowser
import pandas as pd

def open_tab(url):
    webbrowser.get().open(url, new=0, autoraise=False)

def load_database():
    database = pd.read_csv('ennet_database.csv')
    database.set_index("site_num", inplace=True)
    return database

def get_site():
    print('What site would you like to open?')
    return int(raw_input())

def build_url(num, database):
    url_start = 'https://'
    url_domain = str(database.at[num, 'account_sub'])
    url_mid = '.enneteye.jp/sites/'
    url_end = str(num)
    url_english = '?locale=en'
    #url_final = url_start + url_domain + url_mid + url_end
    url_final = url_start + url_domain + url_mid + url_end + url_english
    print(num, database.at[num, 'notes'])
    return url_final

def reco_gen(site_num):
    print('Would you like tp write a reco?')
    print('answer: y OR n')
    reco = str(raw_input())
    if reco is 'y':
        write_reco(site_num)

def write_reco(site_num):
    print(str(site_num) + ' has a recco')

def start():
    print('Should we do it?')
    return str(raw_input())

def main():

    print('Hello')

    database = load_database()
    print("How many tabs do you want")
    tabs = int(raw_input())

    #i = 0
    #while i < 100:
    #    site_num = get_site()
    #    url = str(build_url(site_num, database))
    #    open_tab(url)
    #    reco_gen(site_num)
    #    i = i + 1

    i = 29

    while i < 170:
        go = start()
        j = 0
        while j < tabs:
            if i <= 170:
                site_num = i
                url = str(build_url(site_num, database))
                open_tab(url)
                #reco_gen(site_num)
                i = i + 1
            j = j + 1

main()
