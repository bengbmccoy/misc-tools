### A script that will open the Ennet sites on the correct page which
### contains all of the charts and information

import webbrowser
from datetime import date, timedelta
import pandas as pd

def open_tab(url):
    webbrowser.get().open(url, new=0, autoraise=False)

def load_database():
    database = pd.read_csv('ennet_database.csv')
    database.set_index("site_num", inplace=True)
    return database

def get_site():
    print('What site would you like to open?')
    return int(raw_input())

def build_url(num, database, days):
    start_date = str(date.today() - timedelta(days))
    end_date = str(date.today())
    url_start = 'https://'
    url_domain = str(database.at[num, 'account_sub'])
    url_mid = '.enneteye.jp/sites/'
    url_end = str(num)
    url_date = '?start=' + start_date + '&finish=' + end_date
    url_english = '&locale=en'
    #url_final = url_start + url_domain + url_mid + url_end
    url_final = url_start + url_domain + url_mid + url_end + url_date + url_english
    print(num, database.at[num, 'notes'])
    return url_final

def reco_gen(site_num):
    print('Would you like tp write a reco?')
    print('answer: y OR n')
    reco = str(raw_input())
    if reco is 'y':
        write_reco(site_num)

def write_reco(site_num):
    print(str(site_num) + ' has a recco')

def start():
    print('Should we do it?')
    return str(raw_input())

def main():

    print('Hello')

    #update this to the last site in ennet
    last = 174

    database = load_database()
    #   print database
    print("Where do you want to start?")
    first = int(raw_input())
    if first >= last:
    	first = last
    if first <=0:
    	first = 1

    print("How many tabs at a time?")
    tabs = int(raw_input())

    print("Do you want to include inactive sites?")
    print('y or n')
    inactive = str(raw_input())

    print("How many days do you want?")
    days = int(raw_input())

    i = first

    while i <= last:
        go = start()
        if go is 'q':
            break
        else:
            j = 0
            while j < tabs:
                if i <= last:
                    site_num = i
                    #if database.at[site_num,'notes'] != 'inactive site' or database.at[site_num,'notes'] != 'ded':
                    if database.at[site_num,'notes'] not in ['ded', 'inactive site'] or inactive is 'y':
                    	url = str(build_url(site_num, database, days))
                    	open_tab(url)
                    	j = j + 1
                    #reco_gen(site_num)
                    i = i + 1
                else:
                    j = j + 1

    print("All Done!")

main()
